#!/bin/bash -ex

HOSTNAME_FQDN=$1

TMP=`mktemp -d /tmp/XXXXXXXXX`
cp -r etc ${TMP}
grep -rl '${HOSTNAME_FQDN}' ${TMP} | xargs sed -i "s/\${HOSTNAME_FQDN}/${HOSTNAME_FQDN}/g"

if [[ ${HOSTNAME_FQDN} == *test.* ]] ; then
	BANNER="
		<div style='background: red; color: yellow; text-align: center; font-weight: 900;'>
			WARNING: THIS IS A TEST INSTANCE. DATA CAN VANISH AT ANY TIME.
		</div>"
	echo "${BANNER}" > ${TMP}/etc/gitea/templates/custom/body_outer_pre.tmpl
	echo "Disallow: /" > ${TMP}/etc/gitea/public/robots.txt
fi

rsync -av -e ssh ${TMP}/etc root@${HOSTNAME_FQDN}:/
rsync -av -e ssh --delete ${TMP}/etc/gitea/public root@${HOSTNAME_FQDN}:/etc/gitea/
ssh root@${HOSTNAME_FQDN} chown -R git.git /etc/gitea
rm -rf ${TMP}

